import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import * as rx from 'rxjs';

export interface CanComponentDeactivate {

    canDeactivate: () => rx.Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

    canDeactivate(component: CanComponentDeactivate) {

        return component.canDeactivate ? component.canDeactivate() : true;
    }

}
