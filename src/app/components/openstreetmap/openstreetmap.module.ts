import { NgModule } from '@angular/core';
import { OpenStreetMapComponent } from './openstreetmap.component';
import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { OpenStreetMapService } from '../../services/openstreetmap.service';

@NgModule({
    declarations: [
        OpenStreetMapComponent
    ],
    imports: [
        CommonModule,
        LeafletModule.forRoot()
    ],
    exports: [
        OpenStreetMapComponent
    ],
    providers: [
        OpenStreetMapService
    ]
})
export class OpenStreetMapModule { }
