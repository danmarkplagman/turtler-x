import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
    selector: 'app-openstreetmap',
    templateUrl: 'openstreetmap.component.html'
})

export class OpenStreetMapComponent implements OnInit, AfterViewInit {

    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }
}
