import { Component, OnInit } from '@angular/core';
import { BroadcasterService } from '../../services/broadcaster.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html'
})

export class HeaderComponent implements OnInit {

    public showNotif: boolean;

    constructor(
        private router: Router,
        private broadcasterService: BroadcasterService
    ) {

    }

    ngOnInit() {

    }

    public closeFold(event: Event): void {

        event.preventDefault();

        if (this.router.url.includes('bookmarks')) {

            this.broadcasterService.broadcast('CloseFoldEvent', 'bookmarks');
        }
    }
}
