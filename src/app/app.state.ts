import { Store } from './models/store';

export interface AppState {
    readonly store: Store[];
}
