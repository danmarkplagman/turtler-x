import * as StoreActions from './../actions/store.action';
import { Store } from '../models/store';

const initialState: Store = {
    key: 'name',
    value: 'Dan'
};

export function reducer(state: Store[] = [initialState], action: StoreActions.Actions) {

    switch (action.type) {
        case StoreActions.ADD_DATA:
            return [...state, action.payload];
        case StoreActions.REMOVE_DATA:
            state.splice(action.payload, 1);
            return state;
        default:
            return state;
    }
}
