import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, HostListener, AfterViewInit } from '@angular/core';
import { trigger, state, style, transition, animate, query } from '@angular/animations';
import { Router } from '@angular/router';
import { Store as RxStore } from '@ngrx/store';
import { BroadcasterService } from '../../services/broadcaster.service';
import { Store } from '../../models/store';
import { AppState } from '../../app.state';
import * as rx from 'rxjs';
import * as $ from 'jquery';
import { OpenStreetMapService } from '../../services/openstreetmap.service';

@Component({
    selector: 'app-bookmark',
    templateUrl: './bookmark.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slideInOut', [
            state('in', style({
                height: 'calc(100vh - 57px);',
                width: '333px',
                padding: '10px'
            })),
            state('out', style({
                opacity: '0',
                height: 'calc(100vh - 57px);',
                width: '0px',
                padding: '0px'
            })),
            transition('in => out', animate('200ms ease-in-out'))
        ])
    ]
})
export class BookmarkComponent implements OnInit, AfterViewInit, OnDestroy {

    public fold: string;
    public store: rx.Observable<Store[]>;

    constructor(
        private router: Router,
        private rxStore: RxStore<AppState>,
        private changeDetector: ChangeDetectorRef,
        private broadcasterService: BroadcasterService,
        private openStreetMapService: OpenStreetMapService
    ) { }

    ngOnInit() {

        this.fold = 'in';
        this.changeDetector.detectChanges();
    }

    ngAfterViewInit() {

        this.getBroadcasterEvents();

        this.openStreetMapService.map.invalidateSize({ animate: true, duration: 2, easeLinearity: 2 });
    }

    ngOnDestroy() {

        this.openStreetMapService.map.invalidateSize({ animate: true, duration: 2, easeLinearity: 2 });
    }

    public closeFold(event?: Event): void {

        if (event) {
            event.preventDefault();
        }

        this.fold = 'out';

        setTimeout(() => {
            this.router.navigate(['']);
            this.fold = 'in';
        }, 200);
    }

    private getBroadcasterEvents(): void {

        this.store = this.rxStore.select('store');

        this.store.subscribe((data) => {

            data.forEach(obj => {

                if (obj.key === 'CloseFoldEvent') {

                    if (obj.value === 'bookmarks') {

                        this.closeFold();
                        this.changeDetector.detectChanges();
                    }
                }
            });
        });
    }

}
