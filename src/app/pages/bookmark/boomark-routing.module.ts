import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookmarkComponent } from './bookmark.component';
import { CanDeactivateGuard } from '../../guards/can-deactivate.guard';

export const routes: Routes = [{
    path: '', component: BookmarkComponent
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class BookmarkRoutingModule {}
