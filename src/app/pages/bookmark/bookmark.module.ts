import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookmarkRoutingModule } from './boomark-routing.module';
import { BookmarkComponent } from './bookmark.component';
import { CanDeactivateGuard } from '../../guards/can-deactivate.guard';

@NgModule({
    declarations: [
        BookmarkComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        BookmarkRoutingModule
    ],
    providers: [
        CanDeactivateGuard
    ]
})
export class BookmarkModule {}
