import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

export const routes: Routes = [{
    path: '', component: HomeComponent, children: [
        { path: 'bookmarks', loadChildren: '../bookmark/bookmark.module#BookmarkModule' },
        { path: 'bookmark', children: [
            { path: '', redirectTo: '/bookmarks', pathMatch: 'full' }
        ] },
    ]
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomeRoutingModule {}
