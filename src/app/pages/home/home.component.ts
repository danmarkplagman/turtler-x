import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, OnChanges } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { OpenStreetMapService } from '../../services/openstreetmap.service';
import { NavigationRepository } from '../../repositories/navigation.repository';
import { MapRepository } from '../../repositories/map.repository';
import { delay } from 'q';
import * as L from 'leaflet';

const { version: appVersion } = require('../../../../package.json');

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slideInOut', [
            state('in', style({
                height: 'calc(100vh - 57px);',
                width: '333px',
                padding: '10px'
            })),
            state('out', style({
                opacity: '0',
                height: 'calc(100vh - 57px);',
                width: '0px',
                padding: '0px'
            })),
            transition('out => in', animate('200ms ease-in-out'))
        ])
    ]
})
export class HomeComponent implements OnInit, AfterViewInit, OnChanges {

    public appVersion: number;
    public fold: string;
    public page: string;

    private map: L.Map;

    constructor(
        private router: Router,
        private changeDetector: ChangeDetectorRef,
        private openStreetMapService: OpenStreetMapService
    ) {

    }

    ngOnInit() {

        this.appVersion = appVersion;
        this.fold = 'out';
        this.initMap();
        this.changeDetector.detectChanges();
    }

    ngAfterViewInit() {

        this.openStreetMapService.map.invalidateSize({ animate: true, duration: 2, easeLinearity: 2 });
    }

    ngOnChanges() {

        this.changeDetector.detectChanges();
    }

    public preload(event: Event, routeName: string): void {

        event.preventDefault();

        if (this.router.url.includes('bookmarks')) {
            return;
        }

        this.fold = 'in';

        this.setPage(routeName);

        setTimeout(() => {
            this.router.navigate(['/' + routeName]);
            this.fold = 'out';
        }, 200);
    }

    private setPage(page: string): void {

        this.page = page;
    }

    private initMap(): void {

        this.map = this.openStreetMapService.initializeMap();

        NavigationRepository.getDeviceCurrentLocation();
        NavigationRepository.getDeviceCurrentLocationEveryTenMinutes();

        this.openStreetMapService.map = this.map;
        this.openStreetMapService.map.fitWorld();

        MapRepository.initMapOptions(this.map);
        MapRepository.initZoomControls(this.map);

        this.map.whenReady(() => {

            if (localStorage.getItem('cl_0') && localStorage.getItem('cl_1')) {

                this.openStreetMapService.map.setView(L.latLng(parseFloat(localStorage.getItem('cl_0')), parseFloat(localStorage.getItem('cl_1'))), 14);
            }

            delay(1).then(() => {

                this.initMapLocatedEvent();
                this.map.invalidateSize();
            });
        });
    }

    private initMapLocatedEvent(): void {

        this.openStreetMapService.map.invalidateSize({ animate: true, duration: 2, easeLinearity: 2 });
    }

}
