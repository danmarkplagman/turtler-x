export interface Store {

    key: string;
    value: string;
}
