import { BookmarkTag } from './bookmark-tag';
import { BookmarkGallery } from './bookmark-gallery';

export interface Bookmark {

    id: number;
    tags: Array<BookmarkTag>;
    galleries: Array<BookmarkGallery>;
}
