import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
    { path: '',  loadChildren: './pages/home/home.module#HomeModule' },
    // { path: '**', pathMatch: 'full', loadChildren: './components/not-found/not-found.module#NotFoundModule' },
    // { path: '***', pathMatch: 'full', loadChildren: './components/not-found/not-found.module#NotFoundModule' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            onSameUrlNavigation: 'ignore'
        })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
