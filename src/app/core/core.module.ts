import { NgModule, Optional, SkipSelf } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OpenStreetMapService } from '../services/openstreetmap.service';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../reducers/store.reducer';
import { BroadcasterService } from '../services/broadcaster.service';

export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {

    if (parentModule) {
        throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
    }
}

@NgModule({
    declarations: [

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({
            store: reducer
        })
    ],
    exports: [

    ],
    providers: [
        BroadcasterService
    ]
})
export class CoreModule {

    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {

        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
