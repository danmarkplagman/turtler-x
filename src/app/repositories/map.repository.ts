import * as L from 'leaflet';

export class MapRepository {

    public static initZoomControls(map: L.Map): void {

        L.control.zoom({ position: 'bottomright' }).addTo(map);
    }

    public static initMapOptions(map: L.Map): void {

        const southWest: L.LatLng = L.latLng(-89.98155760646617, -180);
        const northEast: L.LatLng = L.latLng(89.99346179538875, 180);
        const bounds: L.LatLngBounds = L.latLngBounds(southWest, northEast);

        map.setMaxBounds(bounds);
        map.on('drag', () => {
            map.panInsideBounds(bounds, { animate: false });
        });
    }
}
