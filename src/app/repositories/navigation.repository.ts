export class NavigationRepository {

    private static locationInterval: NodeJS.Timer;

    public static getDeviceCurrentLocation(): void {

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition((data: Position) => {

                if (data.coords) {

                    this.saveDeviceCoordinatesToLocalStorage(data.coords);
                }
            });
        }
    }

    public static getDeviceCurrentLocationEveryTenMinutes(): void {

        clearInterval(this.locationInterval);
        this.locationInterval = setInterval(() => {

            NavigationRepository.getDeviceCurrentLocation();
        }, 600000);
    }

    private static saveDeviceCoordinatesToLocalStorage(coordinates: Coordinates): void {

        localStorage.removeItem('cl_0');
        localStorage.removeItem('cl_1');
        localStorage.setItem('cl_0', coordinates.latitude.toString());
        localStorage.setItem('cl_1', coordinates.longitude.toString());
    }
}
