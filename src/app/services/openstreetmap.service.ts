import { Injectable } from '@angular/core';
import * as L from 'leaflet';

@Injectable()
export class OpenStreetMapService {

    public map: L.Map;

    constructor() {

    }

    public initializeMap(): L.Map {

        return L.map('map', {
            zoomControl: false,
            zoom: 15,
            minZoom: 2,
            maxZoom: 18,
            doubleClickZoom: false,
            worldCopyJump: true,
            layers: [L.tileLayer('https://maps.turtler.io/styles/klokantech-basic/{z}/{x}/{y}.png', {
                attribution: '<a target="_blank" href="https://www.openstreetmap.org">OpenStreetMap</a> | ' +
                    '<a target="_blank" href="https://openmaptiles.org/">OpenMapTiles.org</a> | ' +
                    '<a target="_blank" href="https://turtler.io">Turtler GPS Ltd.</a>'
                }
            )]
        });
    }

    public disableMouseEvent(elementId: string): void {

        const element: HTMLElement = <HTMLElement>document.getElementById(elementId);

        L.DomEvent.disableClickPropagation(element);
        L.DomEvent.disableScrollPropagation(element);
    }
}
