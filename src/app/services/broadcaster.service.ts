import { Injectable } from '@angular/core';
import { Store as RxStore } from '@ngrx/store';
import { AppState } from '../app.state';
import * as StoreAction from '../actions/store.action';
import { Observable } from 'rxjs';
import { Store } from '../models/store';
import { map, filter } from 'rxjs/operators';

@Injectable()
export class BroadcasterService {

    constructor(
        private rxStore: RxStore<any>
    ) { }

    public broadcast(key: string, value?: any) {

        const store = this.rxStore.select('store');

        store.subscribe((data: any) => {

            data.forEach((obj, index) => {

                if (obj['key'] === key) {

                    this.rxStore.dispatch(new StoreAction.RemoveData(index));
                }
            });
        });

        this.rxStore.dispatch(new StoreAction.AddData({
            key: key,
            value: value
        }));
    }

    // public on(key: string): Observable<Store[]> {

    //     const store: Observable<Store[]> = this.rxStore.select('store');
    // }
}
