import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Store } from '../models/store';

export const ADD_DATA     = '[DATA] Add';
export const REMOVE_DATA    = '[DATA] Remove';

export class AddData implements Action {
    readonly type = ADD_DATA;

    constructor(public payload: Store) {}
}

export class RemoveData implements Action {
    readonly type = REMOVE_DATA;

    constructor(public payload: number) {}
}

export type Actions = AddData | RemoveData;
